package structures;

import com.google.common.collect.ImmutableList;

import java.util.Collections;
import java.util.List;

public enum Building {
        DWELLING (8, null, "D"),
        TRADING_POST (4, DWELLING, "TP"),
        TEMPLE (3, TRADING_POST, "TE"),
        STRONGHOLD (1, TRADING_POST, "SH"),
        SANCTUARY (1, TEMPLE, "SA");

        private int numAllowed;
        public Building predecessor;
        public String code;

        Building(int numAllowed, Building predecessor, final String code) {
                this.numAllowed = numAllowed;
                this.predecessor = predecessor;
                this.code = code;
        }

        public static Building fromAbbreviation(String abbreviation) {
            for (Building building : Building.values()) {
                if (building.code.equals(abbreviation)) {
                    return building;
                }
            }
            throw new IllegalArgumentException("cannot find that building");
        }

        public Building getRequirement() {
                return this.predecessor;
        }

        public int getNumAllowed() {
                return numAllowed;
        }

        public List<Building> getUpgrades() {
            switch (this) {
                case DWELLING:
                    return Collections.singletonList(TRADING_POST);
                case TRADING_POST:
                    return ImmutableList.of(TEMPLE, STRONGHOLD);
                case TEMPLE:
                    return Collections.singletonList(SANCTUARY);
            }

            return Collections.emptyList();
        }
}
