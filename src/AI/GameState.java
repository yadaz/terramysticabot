package AI;

import actions.Action;
import actions.advance.AdvanceDigAction;
import actions.advance.AdvanceShipAction;
import actions.conversion.BurnPowerAction;
import actions.power.DiggingPowerActionMeta;
import actions.power.PowerAction;
import actions.primary.PassAction;
import actions.primary.TransformAndBuildActionMeta;
import actions.primary.UpgradeActionMeta;
import board.Board;
import board.BonusTile;
import main.Player;
import move.Move;
import playerResources.Resource;
import playerResources.ResourceSet;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

@SuppressWarnings("ALL")
public class GameState {
    public Player player;
    public Board board;

    public GameState(
            Player player, Board board
    ) {
        this.player = player;
        this.board = board;
    }

    public List<Move> getLegalNextMoves() {
        if (player.isPassed) {
            return Collections.emptyList();
        }
        List<Move> legalMoves = new ArrayList<>();
        legalMoves.add(getPassMove(board.roundNumber));

        //
        // BASIC TRANSFORM AND BUILD
        //
        TransformAndBuildActionMeta transformAndBuildActionMeta = new TransformAndBuildActionMeta(this, 0);
        for (List<Action> actions : transformAndBuildActionMeta.getValidMoves(ResourceSet.empty())) {
            legalMoves.add(new Move(actions));
        }

        //
        // UPGRADE (to anything but dwelling)
        //
        UpgradeActionMeta upgradeActionMeta = new UpgradeActionMeta(this);
        for (List<Action> actions : upgradeActionMeta.getValidMoves(ResourceSet.empty())) {
            legalMoves.add(new Move(actions));
        }

        for (PowerAction powerAction : board.powerActions) {
            int canSpendPower = player.resources.canSpendPower(powerAction.getCost().resources.get(Resource.P3));

            if (powerAction.canTake() && canSpendPower != -1) {
                switch (powerAction.getName()) {
                    case "ACT5":
                        DiggingPowerActionMeta actionMeta = new DiggingPowerActionMeta(this, "ACT5", 1, 4);
                        for (List<Action> actions : actionMeta.getValidMoves(ResourceSet.empty())) {
                            final Move move = new Move(actions);
                            legalMoves.add(move);
                        }
                    case "ACT6":
                        actionMeta = new DiggingPowerActionMeta(this, "ACT6", 2, 6);
                        for (List<Action> actions : actionMeta.getValidMoves(ResourceSet.empty())) {
                            final Move move = new Move(actions);
                            legalMoves.add(move);
                        }
                        break;
                    default:
                        Move move = new Move();
                        move.actions.add(new BurnPowerAction(canSpendPower));
                        move.actions.add(powerAction);
                        legalMoves.add(move);
                }
            }
        }

        // ADVANCE SHIP
        final ResourceSet shipCost = new AdvanceShipAction(this.player).getCost();
        List<List<Action>> conversionActions = this.player.resources.getConversionActions(shipCost);
        if (conversionActions != null) {
            for (List<Action> conversionAction : conversionActions) {
                Move move = new Move(new ArrayList<>(conversionAction));
                move.actions.add(new AdvanceShipAction(this.player));
                legalMoves.add(move);
            }
        }
        // ADVANCE DIG
        final ResourceSet digCost = new AdvanceDigAction(this.player).getCost();
        conversionActions = this.player.resources.getConversionActions(digCost);
        if (conversionActions != null) {
            for (List<Action> conversionAction : conversionActions) {
                Move move = new Move(new ArrayList<>(conversionAction));
                move.actions.add(new AdvanceDigAction(this.player));
                legalMoves.add(move);
            }
        }

        if (!legalMoves.isEmpty()) {
            return legalMoves;
        } else {
            return Collections.emptyList();
        }
    }
/**
    private void addDiggingPowerActions(List<Move> legalActions, List<Tile> possibleExpand) {
        ACT5 act5 = null;
        ACT6 act6 = null;
        for (PowerAction powerAction : board.powerActions) {
            if (powerAction instanceof ACT5) {
                act5 = (ACT5) powerAction;
            }
            if (powerAction instanceof ACT6) {
                act6 = (ACT6) powerAction;
            }
        }
        if (act5 != null) {
            int canSpendPower = player.resources.canSpendPower(act5.getCost().resources.get(Resource.P3));

            if (act5.canTake() && canSpendPower != -1) {
                for (final Tile possibleTile : possibleExpand) {
                    if (player.canDigToHomeTerrain(possibleTile, 1)) {
                        final Move move = new Move();
                        move.actions.add(new BurnPowerAction(canSpendPower));

                        board.powerActions.remove(act5);
                        ACT5 act51 = new ACT5(false, possibleTile.coordinate, possibleTile.name);

                        move.actions.add(act51);
                        board.powerActions.add(act51);

                        final TransformAction transformAction = new TransformAction(player, possibleTile, player.canBuildIgnoreTerrain(possibleTile, Building.DWELLING));
                        transformAction.setFreeDigs(1);

                        if (!player.resources.canSpend(transformAction.getCost())) {
                            continue;
                        }

                        move.actions.add(transformAction);
                        legalActions.add(move);
                    }
                }
            }
        }
        if (act6 != null) {
            int canSpendPower = player.resources.canSpendPower(act6.getCost().resources.get(Resource.P3));

            if (act6.canTake() && canSpendPower != -1) {
                for (final Tile possibleTile : possibleExpand) {
                    if (player.canDigToHomeTerrain(possibleTile, 2)) {
                        final Move move = new Move();
                        move.actions.add(new BurnPowerAction(canSpendPower));

                        board.powerActions.remove(act5);
                        ACT6 act61 = new ACT6(false, possibleTile.coordinate, possibleTile.name);

                        move.actions.add(act61);
                        board.powerActions.add(act61);

                        final TransformAction transformAction = new TransformAction(player, possibleTile, player.canBuildIgnoreTerrain(possibleTile, Building.DWELLING));
                        transformAction.setFreeDigs(2);

                        if (!player.resources.canSpend(transformAction.getCost())) {
                            continue;
                        }

                        move.actions.add(transformAction);
                        legalActions.add(move);
                    }
                }
            }
        }
    }
**/
    public Move getPassMove(int round) {
        Move passMove = new Move();
        passMove.actions.add(new PassAction(player, getBonusTile(), round));
        return passMove;
    }

    public BonusTile getBonusTile() {
        for (BonusTile bonusTile : board.bonusTiles) {
            if (bonusTile.isAvailable()) {
                return bonusTile;
            }
        }
        return board.bonusTiles.get(0);
    }

    //TODO: creates a DEEP COPY of the current game state, then applies the move
    public GameState apply(Move m) {
        Player deepCopyPlayer = this.player.deepCopy();
        Board deepCopyBoard = this.board.deepCopy();

        final GameState gameState = new GameState(deepCopyPlayer, deepCopyBoard);
        m.perform(gameState);

        return gameState;
    }
}
