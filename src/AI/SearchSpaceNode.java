package AI;

import move.Move;
import playerResources.Resource;
import structures.Building;

public class SearchSpaceNode {
    public GameState gameState;
    // This is the move that led us to this state.
    public Move move;

    public SearchSpaceNode(
            GameState gameState,
            Move move
    ) {
        this.gameState = gameState;
        this.move = move;
        score = computeScore();
    }

    public double bestScore = Integer.MIN_VALUE;

    public int computeScore() {
        return 10000 * (gameState.player.buildings.get(Building.DWELLING) + gameState.player.buildings.get(Building.TRADING_POST))
                + 5 * gameState.player.resources.resources.get(Resource.WORKER)
                + gameState.player.resources.resources.get(Resource.COIN);
    }

    // This is NOT VICTORY POINTS
    // This is some arbitrary metric of how "good" this game state is for us. We maximize this value when making our moves.
    public double score;
}
