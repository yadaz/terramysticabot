package AI;

import actions.power.PowerAction;
import actions.power.*;
import board.Board;
import board.BonusTile;
import board.Color;
import board.Tile;
import factions.Cultists;
import factions.Faction;
import main.Player;
import models.FavorTile;
import move.Move;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import playerResources.Resource;
import playerResources.ResourceSet;
import structures.Building;

import java.awt.*;
import java.util.*;
import java.util.List;

public class AI {
    // this player object keeps track of the AI player's state
    private Player player = null;

    private String factionName = "cultists";

    private Board board = new Board(Board.setupBoard());

    public Map<String, Object> state;

    public String getMove(WebDriver webDriver) {
        // update player object from the front end
        updateState(webDriver);

        // generate list of possible moves
        GameState current = new GameState(player, board);
        final SearchSpaceNode root = new SearchSpaceNode(current, null);

        // for each possible move, check "score" at the end of the round?

        // return the first move in the tree that leads to our best state

        long currentT = System.currentTimeMillis();
        SearchSpaceNode bestResult = getBestResult(root, "");
        System.out.println(
                String.format("Spent %d millis finding move", System.currentTimeMillis() - currentT)
        );
        if (board.lastPlayerUnpassed) {
            StringBuilder sb = new StringBuilder();

            String bestResultMoveString = bestResult.move.toString();
            sb.append(bestResultMoveString);
            while (!bestResultMoveString.startsWith("\npass")) {
                bestResult = getBestResult(bestResult, "");
                bestResultMoveString = bestResult.move.toString();
                sb.append(bestResultMoveString);
            }
            return sb.toString();

        } else {
            return bestResult.move.toString();
        }
    }

    public static Map<String, Object> findState(WebDriver webDriver) {
        JavascriptExecutor js = (JavascriptExecutor) webDriver;
        while (true) {
            Map<String, Object> state = (Map<String, Object>) js.executeScript("return state;");
            if (state != null) {
                return state;
            }
        }
    }

    public SearchSpaceNode getBestResult(
            SearchSpaceNode root,
            String planningString
    ) {
        final List<Move> legalNextMoves = root.gameState.getLegalNextMoves();
        if (legalNextMoves.isEmpty()) {
            root.bestScore = root.score;
            return root;
        }

        double bestScore = Double.MIN_VALUE;
        SearchSpaceNode best = root;

        int numCompleted = 0;
        for (Move legalNextMove : legalNextMoves) {
            final GameState newGameState = root.gameState.apply(legalNextMove);
            final SearchSpaceNode nextNode = new SearchSpaceNode(newGameState, legalNextMove);
            final SearchSpaceNode bestResultFromNext = getBestResult(nextNode, planningString + "\n" + nextNode.move.toString());
            if (planningString.isEmpty()) {
                numCompleted++;
                System.out.println(
                        String.format("Completed processing first level move %d of %d: %s", numCompleted, legalNextMoves.size(), legalNextMove.toString()) +
                                "\nScore: " + bestResultFromNext.bestScore
                );
            }

            if (bestResultFromNext.bestScore >= bestScore) {
                best = nextNode;
                bestScore = bestResultFromNext.bestScore;
            }
        }

        best.bestScore = bestScore;
        return best;
    }

    public void updateState(WebDriver webDriver) {
        state = findState(webDriver);
        Board board = getBoardState();
        Player player = createPlayer(board.tiles);

        this.board = board;
        this.player = player;
    }


    public static Map<Resource, Integer> getResources(Map<String, Object> currentFaction) {
        Map<Resource, Integer> resources = new HashMap<>();
        resources.put(Resource.COIN, pullOutValueFromNum(currentFaction.get("C")));
        resources.put(Resource.VICTORY_POINT, pullOutValueFromNum(currentFaction.get("VP")));
        resources.put(Resource.WORKER, pullOutValueFromNum(currentFaction.get("W")));
        resources.put(Resource.PRIEST, pullOutValueFromNum(currentFaction.get("P")));
        resources.put(Resource.P1, pullOutValueFromNum(currentFaction.get("P1")));
        resources.put(Resource.P2, pullOutValueFromNum(currentFaction.get("P2")));
        resources.put(Resource.P3, pullOutValueFromNum(currentFaction.get("P3")));
        resources.put(Resource.PRIESTS_AVAILABLE, pullOutValueFromNum(currentFaction.get("MAX_P")));
        resources.put(Resource.BRIDGE_COUNT, pullOutValueFromNum(currentFaction.get("BRIDGE_COUNT")));
        return resources;
    }

    public Board getBoardState() {
        Board board = new Board(Board.setupBoard());
        Map<String, Map<String, Object>> boardState = ((Map<String, Map<String, Object>>) state.get("map"));
        for (final String name : boardState.keySet()) {
            Map<String, Object> tile = boardState.get(name);
            int row = pullOutValueFromNum(tile.get("col"));
            int column = pullOutValueFromNum(tile.get("row"));
            if ((tile.get("col") != null) && (tile.get("row") != null) && (column != -1) && (row != -1)) {
                Tile newTile = new Tile(new Point(column, row), name, Color.valueOf(((String) tile.get("color")).toUpperCase()));
                if (tile.get("bridgable") != null) {
                    newTile.setBridgable(tile.get("bridgable").equals("1"));
                }
                if (tile.get("has_neighbors") != null) {
                    newTile.setHasNeighbor(tile.get("has_neighbors").equals("1"));
                }
                if (tile.get("building") != null) {
                    newTile.setBuilding(Building.fromAbbreviation((String) tile.get("building")));
                }
                board.setTile(column, row, newTile);
            }
        }
        List<BonusTile> bonusTiles = new ArrayList<>();
        Map<String, String> pool = (Map<String, String>) state.get("pool");
        for (String poolTileName : pool.keySet()) {
            if (poolTileName.startsWith("BON")) {
                Map<String, String> bonusCoin = ((Map<String, Map<String, String>>) state.get("bonus_coins")).get(poolTileName);
                BonusTile bonusTile = new BonusTile(poolTileName,
                        pullOutValueFromNum(bonusCoin.get("C")),
                        pullOutValueFromNum(pool.get(poolTileName)) > 0
                );
                bonusTiles.add(bonusTile);
            }
        }

        List<PowerAction> powerActions = new ArrayList<>();
        for (int i = 1; i <= 6; i++) {
            final Map<String, Object> actionStatus = boardState.get("ACT" + i);

            boolean blocked = actionStatus != null;
            if (actionStatus != null) {
                blocked = actionStatus.get("blocked").equals("1");
            }
            switch (i) {
                case 2:
                    powerActions.add(new ACT2(blocked));
                    break;
                case 3:
                    powerActions.add(new ACT3(blocked));
                    break;
                case 4:
                    powerActions.add(new ACT4(blocked));
                    break;
                case 5:
                    powerActions.add(new ACT5(blocked));
                    break;
                case 6:
                    powerActions.add(new ACT6(blocked));
                    break;
            }
        }
        board.powerActions = powerActions;

        board.setBonusTiles(bonusTiles);
        board.setRoundNumber(pullOutValueFromNum(state.get("round")));
        board.setLastPlayerUnpassed(getLastPlayerUnpassed((Map<String, Map<String, Object>>) state.get("factions")));
        return board;
    }

    public boolean getLastPlayerUnpassed(Map<String, Map<String, Object>> factions) {
        boolean lastPlayerUnpassed = true;
        for (String faction : factions.keySet()) {
            if (!faction.equals(factionName)) {
                Map<String, Object> factionInfo = factions.get(faction);
                if (pullOutValueFromNum(factionInfo.get("passed")) == 0) {
                    lastPlayerUnpassed = false;
                    break;
                }
            }
        }
        return lastPlayerUnpassed;
    }

    public Player createPlayer(Tile[][] tiles) {
        final Faction faction = new Cultists();
        Map<String, Map<String, Object>> factionList = (Map<String, Map<String, Object>>) state.get("factions");
        Map<String, Object> currentFactionInfo = factionList.get(faction.getFactionName());
        Map<Resource, Integer> resources = getResources(currentFactionInfo);
        Map<Building, Integer> buildings = getBuildings(tiles, faction.getHomeTerrain().name());
        List<FavorTile> favorTiles = new ArrayList<>();
        Set<Tile> ownedSpaces = getOwnedSpaces(tiles, faction.getHomeTerrain().name());
        int shippingLevel = pullOutValueFromNum(((Map<String, String>) currentFactionInfo.get("ship")).get("level"));
        int digLevel = pullOutValueFromNum(((Map<String, String>) currentFactionInfo.get("dig")).get("level"));
        return new Player(new ResourceSet(resources), buildings, shippingLevel, digLevel, favorTiles, ownedSpaces, faction, false);
    }

    public void setPlayer(Player player) {
        this.player = player;
    }

    public void setBoard(Board board) {
        this.board = board;
    }


    public static Set<Tile> getOwnedSpaces(Tile[][] tiles, final String color) {
        Set<Tile> ownedTiles = new HashSet<>();
        for (int i = 0; i < tiles.length; i++) {
            Tile[] currentRow = tiles[i];
            for (int j = 0; j < currentRow.length; j++) {
                Tile currentTile = currentRow[j];
                if ((currentTile != null) && (currentTile.getBuilding() != null) && (currentTile.getColor() != null) && (currentTile.getColor().name().equals(color))) {
                    ownedTiles.add(currentTile);
                }
            }
        }
        return ownedTiles;
    }

    public static Map<Building, Integer> getBuildings(Tile[][] tiles, final String color) {
        int dwellingCount = 0;
        int tradingPostCount = 0;
        int templeCount = 0;
        int strongholdCount = 0;
        int sanctuaryCount = 0;
        for (int i = 0; i < tiles.length; i++) {
            Tile[] currentRow = tiles[i];
            for (int j = 0; j < currentRow.length; j++) {
                Tile currentTile = currentRow[j];
                if ((currentTile != null) && (currentTile.getBuilding() != null) && (currentTile.getColor() != null) && (currentTile.getColor().name().equals(color))) {
                    String buildingName = currentTile.getBuilding().name();
                    switch(buildingName) {
                        case "DWELLING": dwellingCount++;
                            break;
                        case "TRADING_POST": tradingPostCount++;
                            break;
                        case "TEMPLE": templeCount++;
                            break;
                        case "STRONGHOLD": strongholdCount++;
                            break;
                        case "SANCTUARY": sanctuaryCount++;
                            break;
                        default:
                            break;
                    }
                }
            }
        }
        Map<Building, Integer> buildings = new HashMap<>();
        buildings.put(Building.DWELLING, dwellingCount);
        buildings.put(Building.TRADING_POST, tradingPostCount);
        buildings.put(Building.TEMPLE, templeCount);
        buildings.put(Building.STRONGHOLD, strongholdCount);
        buildings.put(Building.SANCTUARY, sanctuaryCount);
        return buildings;
    }

    private static int pullOutValueFromNum(Object original) {
        if (original instanceof String) {
            return Integer.parseInt((String)original);
        }
        if (original instanceof Long) {
            return ((Long)original).intValue();
        }
        return -1;
    }


}
