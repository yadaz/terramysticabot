package factions;

import board.Color;
import com.google.common.collect.ImmutableMap;
import main.Player;
import playerResources.Resource;
import playerResources.ResourceSet;
import structures.Building;

import java.util.Map;

public class Fakirs extends Faction {

    @Override
    public ResourceSet getCost(final Building b, boolean isAdjacent) {
        switch (b) {
            case STRONGHOLD:
                return new ResourceSet(ImmutableMap.of(
                        Resource.WORKER, 4,
                        Resource.COIN, 10
                ));
            case SANCTUARY:
                return new ResourceSet(ImmutableMap.of(
                        Resource.WORKER, 4,
                        Resource.COIN, 6
                ));
            default:
                return super.getCost(b, isAdjacent);
        }
    }

    @Override
    public ResourceSet getIncome(final Map<Building, Integer> builtThings) {
        throw new UnsupportedOperationException("You need to implement this");
    }

    @Override
    public Object advanceBuilding(final Building b) {
        throw new UnsupportedOperationException("You need to implement this");
    }

    @Override
    public Color getHomeTerrain() {
        return Color.YELLOW;
    }

    @Override
    public void initPlayer(final Player player) {
        return;
    }

    @Override
    public String getFactionName() {
        return "fakirs";
    }
}
