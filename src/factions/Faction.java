package factions;

import board.Color;
import com.google.common.collect.ImmutableMap;
import main.Player;
import playerResources.Resource;
import playerResources.ResourceSet;
import structures.Building;

import java.util.Map;

public abstract class Faction {

    public ResourceSet getCost(Building b, boolean isAdjacent) {
        switch (b) {
            case DWELLING:
                return new ResourceSet(ImmutableMap.of(
                        Resource.WORKER, 1,
                        Resource.COIN, 2
                ));
            case TRADING_POST:
                if(isAdjacent) {
                    return new ResourceSet(ImmutableMap.of(
                            Resource.WORKER, 2,
                            Resource.COIN, 3
                    ));
                } else {
                    return new ResourceSet(ImmutableMap.of(
                            Resource.WORKER, 2,
                            Resource.COIN, 6
                    ));
                }
            case TEMPLE:
                return new ResourceSet(ImmutableMap.of(
                        Resource.WORKER, 2,
                        Resource.COIN, 5
                ));
            case STRONGHOLD:
                return new ResourceSet(ImmutableMap.of(
                        Resource.WORKER, 4,
                        Resource.COIN, 6
                ));
            case SANCTUARY:
                return new ResourceSet(ImmutableMap.of(
                        Resource.WORKER, 4,
                        Resource.COIN, 6
                ));
            default:
                throw new IllegalArgumentException("hey idiot, that's not a building!");
        }
    }

    // Given that I've built {Dwelling, 3, TradingPost, 2}, how many of each resource do I get?
    public abstract ResourceSet getIncome(
            Map<Building, Integer> builtThings
    );

    // Will eventually be used to identify special things that happen when a faction builds a thing
    public abstract Object advanceBuilding(Building b);

    public abstract Color getHomeTerrain();

    public abstract void initPlayer(final Player player);

    public abstract String getFactionName();

    public int getMaximumShippingLevel() {
        return 3;
    }

    public int getMaximumDiggingLevel() {
        return 2;
    }

    public ResourceSet costForSpade(int digLevel) {
        switch (digLevel) {
            case 0:
                return new ResourceSet(ImmutableMap.of(
                        Resource.WORKER, 3
                ));
            case 1:
                return new ResourceSet(ImmutableMap.of(
                        Resource.WORKER, 2
                ));
            case 2:
                return new ResourceSet(ImmutableMap.of(
                        Resource.WORKER, 1
                ));
            default:
                throw new IllegalArgumentException("hey idiot, dig level is too high!");
        }
    }
}
