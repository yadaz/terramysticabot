package main;

import AI.AI;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

import java.util.List;

public class Main {

    public static void main(String[] args) throws Exception {
        System.setProperty("webdriver.chrome.driver", "src/resources/chromedriver.exe");

        WebDriver webdriver = new ChromeDriver();

        login(webdriver);

        AI ai = new AI();
        // TODO: Add Player info into AI

        while(true) {
            String link = waitForMyTurn(webdriver);
            makeMove(webdriver, link, ai);
        }
    }
    public static void login(WebDriver webDriver) {
        webDriver.get("https://terra.snellman.net/login/");
        WebElement username = webDriver.findElement(By.name("username"));
        username.sendKeys("XEV0C");
        WebElement password = webDriver.findElement(By.name("password"));
        password.sendKeys("password1");
        WebElement login = webDriver.findElement(By.xpath("//input[@value='login']"));
        login.click();
    }

    /**
     * Blocks until it's my turn.
     *
     * Account must already be logged in.
     *
     */
    public static String waitForMyTurn(WebDriver webDriver) throws InterruptedException {
        while(true) {
            webDriver.get("https://terra.snellman.net/");

            WebElement table = webDriver.findElement(By.id("yourgames-active"));
            Thread.sleep(1000);
            List<WebElement> rows = table.findElements(By.tagName("tr"));
            for (WebElement row : rows) {
                List<WebElement> columns = row.findElements(By.tagName("td"));
                if (columns.size() < 7) continue;
                if (columns.get(5).getText().equals("your turn")) {

                    WebElement link = columns.get(1).findElement(By.tagName("a"));
                    return link.getAttribute("href");
                }
            }

            try {
                Thread.sleep(10000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }

    public static boolean makeMove(WebDriver webDriver, String gameLink, AI ai) throws Exception {
        webDriver.get(gameLink);
        Thread.sleep(1000);

        WebElement actionRequiredBox = webDriver.findElement(By.id("action_required"));
        List<WebElement> buttons = actionRequiredBox.findElements(By.tagName("button"));

        while (!buttons.isEmpty()) {
            actionRequiredBox = webDriver.findElement(By.id("action_required"));
            buttons = actionRequiredBox.findElements(By.tagName("button"));
            Thread.sleep(500);
            try {
                for (WebElement button : buttons) {
                    if (button.getText().equals("Accept")) {
                        button.sendKeys("\n");
                        Thread.sleep(500);
                        break;
                    }
                    if (button.getText().equals("Decline")) {
                        button.sendKeys("\n");
                        Thread.sleep(500);
                        break;
                    }
                    button.sendKeys("\n");
                }
                Thread.sleep(500);

            } catch (Exception e) {
                Thread.sleep(1000);
                e.printStackTrace();
            }
        }

        try {
            WebElement previewSaveButton = webDriver.findElement(By.id("move_entry_action"));
            if (previewSaveButton.getText().equals("Save") && !"true".equals(previewSaveButton.getAttribute("disabled"))) {
                previewSaveButton.sendKeys("\n");
                return true;
            }
        } catch ( Exception e) { e.printStackTrace(); }

        Thread.sleep(2000);

        for (int i = 0; i < 15; i++) {
            i++;
            final String move = ai.getMove(webDriver);
            final WebElement moveTextBox = webDriver.findElement(By.id("move_entry_input"));
            Thread.sleep(2000);
            moveTextBox.sendKeys(move);

            Thread.sleep(2000);
            for (int j = 0; j < 10; j++) {
                List<WebElement> previewSaveButtons;
                for (int k = 0; k < 10; k++) {
                    previewSaveButtons = webDriver.findElements(By.id("move_entry_action"));
                    boolean ok = false;
                    for (WebElement previewSaveButton : previewSaveButtons) {
                        previewSaveButton.sendKeys("\n");
                        if (previewSaveButton.getText().equals("Save")) {
                            ok = true;
                        }
                    }

                    if (ok) {
                        break;
                    }
                    Thread.sleep(500);
                }

                Thread.sleep(500);

                actionRequiredBox = webDriver.findElement(By.id("action_required"));
                buttons = actionRequiredBox.findElements(By.tagName("button"));

                while (!buttons.isEmpty()) {
                    buttons = actionRequiredBox.findElements(By.tagName("button"));
                    Thread.sleep(2000);
                    try {
                        for (WebElement button : buttons) {
                            if (button.getText().equals("Accept")) {
                                button.sendKeys("\n");
                                Thread.sleep(3000);
                                break;
                            }
                            if (button.getText().equals("Decline")) {
                                button.sendKeys("\n");
                                Thread.sleep(3000);
                                break;
                            }
                            button.sendKeys("\n");
                        }
                        Thread.sleep(3000);

                    } catch (Exception e) {
                        Thread.sleep(1000);
                        e.printStackTrace();
                    }
                }

                try {
                    WebElement previewSaveButton = webDriver.findElement(By.id("move_entry_action"));
                    if (previewSaveButton.getText().equals("Save") && !"true".equals(previewSaveButton.getAttribute("disabled"))) {
                        previewSaveButton.sendKeys("\n");
                        return true;
                    }
                } catch ( Exception e) { e.printStackTrace(); }
                return true;
            }
        }

        return false;
    }
}
