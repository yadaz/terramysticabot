package main;

import board.Tile;
import factions.Faction;
import models.FavorTile;
import playerResources.Resource;
import playerResources.ResourceSet;
import structures.Building;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

//Represents the game state of the bots board
public class Player {
    public ResourceSet resources = new ResourceSet(new HashMap<Resource, Integer>());
    public Map<Building, Integer> buildings = new HashMap<>();

    public int shipLevel;
    public int digLevel;
    public List<FavorTile> favorTiles;

    public Set<Tile> ownedSpaces;

    public boolean isPassed;

    public int[] cultPlace; //TODO

    public final Faction faction;

    public Player(
            Faction faction
    ) {
        this.faction = faction;
        faction.initPlayer(this);
    }

    public Player(
            ResourceSet resources,
            Map<Building, Integer> buildings,
            int shipLevel,
            int digLevel,
            List<FavorTile> favorTiles,
            Set<Tile> ownedSpaces,
            Faction faction,
            boolean isPassed
    ) {
        this.faction = faction;
        this.resources = resources;
        this.buildings = buildings;
        this.shipLevel = shipLevel;
        this.digLevel = digLevel;
        this.favorTiles = favorTiles;
        this.ownedSpaces = ownedSpaces;
        this.isPassed = isPassed;
    }

    /**
    public boolean canBuild(Tile tileToBuild, Building buildingToBuild) {
        //Check if the tile is the correct
        if (tileToBuild.getColor() != faction.getHomeTerrain()) {
            return false;
        }

        return canBuildIgnoreTerrain(tileToBuild, buildingToBuild);
    }

    public boolean canBuildIgnoreTerrain(Tile tileToBuild, Building buildingToBuild) {

        final ResourceSet cost = faction.getCost(buildingToBuild, tileToBuild.getHasNeighbor());
        // do I have enough shit?
        final boolean canSpend = resources.canSpend(cost);
        // can build dwelling

        //Do we still have any buildings of this type left to build
        boolean enoughBuildings = !(buildings.get(buildingToBuild) >= buildingToBuild.getNumAllowed());

        if (!(canSpend && enoughBuildings)) {
            return false;
        }

        if (buildingToBuild == Building.DWELLING) {
            if (tileToBuild.building == null) {
                return true;
            }
        } else {
            if (tileToBuild.building == null) {
                return false;
            }
            return buildingToBuild.getRequirement() == tileToBuild.building;
        }

        return false;
    }

    public void build(Tile tileToBuild, Building buildingToBuild) {
        if (!canBuild(tileToBuild, buildingToBuild)) {
            throw new IllegalArgumentException("can't build here!");
        }
        final ResourceSet cost = faction.getCost(buildingToBuild, tileToBuild.getHasNeighbor());
        this.resources.update(cost, false);

        tileToBuild.building = buildingToBuild;
        buildings.put(buildingToBuild, buildings.get(buildingToBuild) + 1);
        if(buildingToBuild.predecessor != null) {
            buildings.put(buildingToBuild.predecessor, buildings.get(buildingToBuild.predecessor) - 1);
        }
        ownedSpaces.remove(tileToBuild);
        ownedSpaces.add(tileToBuild);
    }

    public boolean canDigToHomeTerrain(Tile tileToDig, int freeDigs) {
        if(tileToDig.building != null) {
            return false;
        }
        if(!resources.canSpend(costToDigHomeTerrain(tileToDig, freeDigs))) {
          return false;
        }
        return true;
    }

    public ResourceSet costToDigHomeTerrain(Tile tileToDig, int freeDigs) {
        int spadesNeeded = faction.getHomeTerrain().getMinDistanceTo(tileToDig.getColor());
        spadesNeeded = Math.max(0, spadesNeeded - freeDigs);
        final ResourceSet cost = faction.costForSpade(digLevel);
        if (spadesNeeded == 0) { return new ResourceSet(new HashMap<Resource, Integer>()); }
        for(final Resource resource : cost.resources.keySet()) {
            cost.update(resource, cost.resources.get(resource) * (spadesNeeded - 1));
        }
        return cost;
    }

    public void digToHomeTerrain(Tile tileToDig, int freeDigs) {
        final ResourceSet cost = costToDigHomeTerrain(tileToDig, freeDigs);
        resources.update(cost, false);
        tileToDig.setColor(faction.getHomeTerrain());
    }**/

    public Player deepCopy() {
        List<FavorTile> clonedFavorTiles = new ArrayList<>();
        Set<Tile> clonedTiles = new HashSet<>();
        for(final FavorTile tile : favorTiles) {
            clonedFavorTiles.add(tile.deepClone());
        }

        for(final Tile tile : ownedSpaces) {
            clonedTiles.add(tile.deepCopy());
        }

        return new Player(
                new ResourceSet(new HashMap<>(resources.resources)),
                new HashMap<>(buildings),
                shipLevel,
                digLevel,
                clonedFavorTiles,
                clonedTiles,
                faction,
                isPassed
        );
    }
}