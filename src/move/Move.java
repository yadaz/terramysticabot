package move;

import AI.GameState;
import actions.Action;

import java.util.ArrayList;
import java.util.List;

public class Move {
    public List<Action> actions;

    public Move() {
        actions = new ArrayList<>();
    }

    public Move(List<Action> actions) {
        this.actions = actions;
    }

    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("\n");

        for (Action action : actions) {
            sb.append(action.toString());
            sb.append("\n");
        }

        return sb.toString();
    }

    public void perform(GameState gameState) {
        for (Action action : actions) {
            action.perform(gameState);
        }
    }
}
