package actions;

import AI.GameState;
import playerResources.ResourceSet;

import java.util.List;

public abstract class ActionMeta {

    public GameState gameState;

    public ActionMeta(GameState gameState) {
        this.gameState = gameState;
    }

    public abstract List<List<Action>> getValidMoves(ResourceSet alreadySpentResources);

    public abstract ActionMeta copy(GameState gameState);
}