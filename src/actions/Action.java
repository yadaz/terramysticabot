package actions;

import AI.GameState;
import main.Player;
import playerResources.ResourceSet;

public abstract class Action {
    protected Player player;
    public Action(Player player) {
        this.player = player;
    }

    public abstract ResourceSet getCost();

    public abstract void perform(GameState gameState);

    public abstract boolean isFinal();
}
