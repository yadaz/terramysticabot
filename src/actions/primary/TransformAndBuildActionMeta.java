package actions.primary;

import AI.GameState;
import actions.Action;
import actions.ActionMeta;
import board.Tile;
import playerResources.ResourceSet;
import structures.Building;

import java.util.ArrayList;
import java.util.List;

public class TransformAndBuildActionMeta extends ActionMeta {
    public final int numFreeDigs;
    public boolean noDigOk = false;

    public TransformAndBuildActionMeta(final GameState gameState, int numFreeDigs) {
        super(gameState);
        this.numFreeDigs = numFreeDigs;
    }

    // After ACT5, it's OK to "dig 0" and pass
    // Otherwise, it's not OK
    public void setAllowNoDig() {
        noDigOk = true;
    }

    @Override
    public List<List<Action>> getValidMoves(final ResourceSet alreadySpentResources) {
        List<List<Action>> validMoves = new ArrayList<>();

        List<Tile> possibleExpand = gameState.board.getReachableAreas(gameState.player);
        for (Tile tile : possibleExpand) {
            if (tile.building == null) {
                final TransformAction transformAction = new TransformAction(gameState.player, tile);
                transformAction.freeDigs = numFreeDigs;

                ResourceSet totalCost = alreadySpentResources.copy();
                // first, add a move that's only a transform to home terrain
                totalCost.update(transformAction.getCost(), true);
                List<List<Action>> conversionActionsForTerraform = gameState.player.resources.getConversionActions(totalCost);
                if (conversionActionsForTerraform == null) {
                    continue; // if there's not enough to terraform to my home, can't build this tile, ignore
                }
                if (noDigOk || !tile.color.equals(gameState.player.faction.getHomeTerrain())) {
                    for (List<Action> conversionActions : conversionActionsForTerraform) {
                        List<Action> sequence = new ArrayList<>();

                        sequence.addAll(conversionActions);
                        sequence.add(transformAction);
                        validMoves.add(sequence);
                    }
                }

                // Now, try to build a dwelling in addition to the terraform
                if (gameState.player.buildings.get(Building.DWELLING) >= Building.DWELLING.getNumAllowed()) { continue; }
                final UpgradeAction upgradeAction = new UpgradeAction(gameState.player, tile, Building.DWELLING);
/**
                if (gameState.player.resources.resources.get(Resource.WORKER) >= 3 && tile.color.equals(Color.BROWN)) {
                    System.out.println("aaaa");
                }**/
                totalCost.update(upgradeAction.getCost(), true);

                List<List<Action>> conversionActionsForTandB = gameState.player.resources.getConversionActions(totalCost);

                if (conversionActionsForTandB == null) {
                    continue; // if there's not enough to terraform to my home, can't build this tile, ignore
                }
                for (List<Action> conversionActions : conversionActionsForTandB) {
                    ArrayList<Action> sequence = new ArrayList<>();

                    sequence.addAll(conversionActions);
                    sequence.add(transformAction);
                    sequence.add(new UpgradeAction(gameState.player, tile, Building.DWELLING));
                    validMoves.add(sequence);
                }
            }
        }


        return validMoves;
    }

    @Override
    public ActionMeta copy(final GameState gameState) {
        throw new UnsupportedOperationException("You need to implement this");
    }
}
