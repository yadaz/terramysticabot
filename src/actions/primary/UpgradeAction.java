package actions.primary;

import AI.GameState;
import actions.Action;
import board.Tile;
import main.Player;
import playerResources.ResourceSet;
import structures.Building;

public class UpgradeAction extends Action {

    private final Tile tile;
    private final Building upgrade;

    public UpgradeAction(
            Player player, Tile tile, Building upgrade
    ) {
        super(player);
        this.tile = tile;
        this.upgrade = upgrade;
    }

    @Override
    public ResourceSet getCost() {
        return player.faction.getCost(upgrade, tile.hasNeighbor);
    }

    @Override
    public void perform(final GameState gameState) {
        Tile realSpaceToBuild = gameState.board.tiles[tile.coordinate.x][tile.coordinate.y];

        if (gameState.player.buildings.get(upgrade) > upgrade.getNumAllowed()) {
            throw new IllegalArgumentException("Trying to build too many of " + upgrade.toString());
        }

        if (!(realSpaceToBuild.color.equals(player.faction.getHomeTerrain()))) {
            throw new IllegalArgumentException("Can only build on my home terrain");
        }

        if (upgrade.getRequirement() != null && !(upgrade.getRequirement().equals(realSpaceToBuild.building))) {
            throw new IllegalArgumentException("Building requirement not satisfied");
        }

        ResourceSet cost = (new UpgradeAction(gameState.player, realSpaceToBuild, upgrade)).getCost();
        gameState.player.resources.update(cost, false);

        if (gameState.player.buildings.get(upgrade) == null) {
            gameState.player.buildings.put(upgrade, 0);
        }

        gameState.player.buildings.put(
                upgrade,
                (gameState.player.buildings.get(upgrade)) + 1
        );

        if (realSpaceToBuild.building != null) {
            gameState.player.buildings.put(
                    realSpaceToBuild.building,
                    (gameState.player.buildings.get(realSpaceToBuild.building)) - 1
            );
        }

        if (upgrade.equals(Building.DWELLING)) {
            gameState.player.ownedSpaces.add(realSpaceToBuild);
        }

        realSpaceToBuild.building = upgrade;
    }

    @Override
    public boolean isFinal() {
        return true;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();

        if (upgrade.equals(Building.DWELLING)) {
            sb.append("build ").append(tile.getName());
        } else {
            sb.append("upgrade " + tile.getName() + " to " + upgrade.code);
        }
        return sb.toString();
    }
}
