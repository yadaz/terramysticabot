package actions.primary;

import AI.GameState;
import actions.Action;
import board.BonusTile;
import main.Player;
import playerResources.ResourceSet;
import sun.reflect.generics.reflectiveObjects.NotImplementedException;

public class PassAction extends Action {

    private BonusTile bonusTile;
    private int round;

    public PassAction(Player player, BonusTile bonusTile, int round) {
        super(player);
        this.bonusTile = bonusTile;
        this.round = round;
    }

    @Override
    public ResourceSet getCost() {
        //This method is never used currently
        throw new NotImplementedException();
    }

    @Override
    public void perform(GameState gameState) {
        gameState.player.isPassed = true;
    }

    @Override
    public boolean isFinal() {
        return true;
    }

    @Override
    public String toString() {
        if (round < 6) {
            return "pass " + bonusTile.getTileName();
        } else {
            return "pass";
        }
    }
}
