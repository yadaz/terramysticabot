package actions.primary;

import AI.GameState;
import actions.Action;
import board.Tile;
import main.Player;
import playerResources.ResourceSet;

public class TransformAction extends Action {
    public Tile spaceToBuild;

    public int numDigs;
    public int freeDigs = 0;

    public TransformAction(
            Player player,
            final Tile spaceToBuild
    ) {
        super(player);
        this.numDigs = player.faction.getHomeTerrain().getMinDistanceTo(spaceToBuild.getColor());
        this.spaceToBuild = spaceToBuild;
    }

    @Override
    public ResourceSet getCost() {
        final ResourceSet costPerSpade = player.faction.costForSpade(player.digLevel);
        final ResourceSet totalCost = ResourceSet.empty();

        int minDistance = player.faction.getHomeTerrain().getMinDistanceTo(spaceToBuild.color);
        if (minDistance == 0) {
            return ResourceSet.empty();
        }

        for (int i = 0; i < minDistance; i++) {
            totalCost.update(costPerSpade, true);
        }

        return totalCost;
    }

    public TransformAction setFreeDigs(int freeDigs) {
        this.freeDigs = freeDigs;
        return this;
    }

    @Override
    public void perform(GameState gameState) {
        Tile realSpaceToBuild = gameState.board.tiles[spaceToBuild.coordinate.x][spaceToBuild.coordinate.y];

        ResourceSet cost = new TransformAction(gameState.player, realSpaceToBuild).getCost();

        gameState.player.resources.update(cost, false);
        realSpaceToBuild.color = player.faction.getHomeTerrain();
    }

    @Override
    public boolean isFinal() {
        return true;
    }

    @Override
    public String toString() {

        StringBuilder sb = new StringBuilder();
        if (numDigs - freeDigs > 0) {
            sb.append("dig " + (numDigs - freeDigs));
            sb.append("\n");

            sb.append("transform " + spaceToBuild.getName() + " to " + player.faction.getHomeTerrain());
        }
        return sb.toString();
    }
}
