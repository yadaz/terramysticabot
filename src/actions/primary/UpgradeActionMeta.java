package actions.primary;

import AI.GameState;
import actions.Action;
import actions.ActionMeta;
import board.Tile;
import playerResources.ResourceSet;
import structures.Building;

import java.util.ArrayList;
import java.util.List;

public class UpgradeActionMeta extends ActionMeta {

    public UpgradeActionMeta(final GameState gameState) {
        super(gameState);
    }

    @Override
    public List<List<Action>> getValidMoves(final ResourceSet alreadySpentResources) {
        List<List<Action>> validActions = new ArrayList<>();
        for (Tile ownedSpace : gameState.player.ownedSpaces) {
            ownedSpace = gameState.board.tiles[ownedSpace.coordinate.x][ownedSpace.coordinate.y];
            for (Building upgrade : ownedSpace.building.getUpgrades()) {
                if (upgrade == null) {continue;}
                ResourceSet totalCost = alreadySpentResources.copy();
                ResourceSet cost = (new UpgradeAction(gameState.player, ownedSpace, upgrade)).getCost();
                totalCost.update(cost, true);

                List<List<Action>> conversionActions = gameState.player.resources.getConversionActions(totalCost);
                if (conversionActions == null) {
                    continue;
                }

                for (List<Action> conversionAction : conversionActions) {
                    List<Action> sequence = new ArrayList<>();
                    sequence.addAll(conversionAction);
                    sequence.add(new UpgradeAction(gameState.player, ownedSpace, upgrade));
                    validActions.add(sequence);
                }
            }
        }
        return validActions;
    }

    @Override
    public ActionMeta copy(final GameState gameState) {
        throw new IllegalArgumentException("Not allowed to copy this");
    }
}
