package actions.conversion;

import AI.GameState;
import actions.Action;
import com.google.common.collect.ImmutableMap;
import playerResources.Resource;
import playerResources.ResourceSet;

public class BurnPowerAction extends Action {

    int numToBurn;

    public BurnPowerAction(int amountToBurn) {
        super(null);
        this.numToBurn = amountToBurn;
    }

    @Override
    public ResourceSet getCost() {
        return new ResourceSet(ImmutableMap.<Resource, Integer>of());
    }

    @Override
    public void perform(GameState gameState) {
        int playerP2 = gameState.player.resources.resources.get(Resource.P2);
        int playerP3 = gameState.player.resources.resources.get(Resource.P3);

        if (playerP2 < 2 * numToBurn) {
            throw new IllegalArgumentException("cannot burn that much power");
        }

        playerP2 -= numToBurn * 2;
        playerP3 += numToBurn;

        gameState.player.resources.resources.put(Resource.P2, playerP2);
        gameState.player.resources.resources.put(Resource.P3, playerP3);
    }

    @Override
    public boolean isFinal() {
        return false;
    }

    @Override
    public String toString() {
        if (numToBurn > 0) {
            return "burn " + numToBurn;
        }
        return "";
    }
}
