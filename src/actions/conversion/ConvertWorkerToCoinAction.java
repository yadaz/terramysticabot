package actions.conversion;

import AI.GameState;
import actions.Action;
import com.google.common.collect.ImmutableMap;
import playerResources.Resource;
import playerResources.ResourceSet;

public class ConvertWorkerToCoinAction extends Action {

    private final int times;

    public ConvertWorkerToCoinAction(int times) {
        super(null);
        this.times = times;
    }

    @Override
    public ResourceSet getCost() {
        return new ResourceSet(ImmutableMap.<Resource, Integer>of());
    }

    @Override
    public void perform(GameState gameState) {
        final int workers = gameState.player.resources.resources.get(Resource.WORKER);

        if (workers < times) {
            throw new IllegalArgumentException("Not enough workers to convert");
        }

        gameState.player.resources.resources.put(Resource.WORKER, workers - times);
        gameState.player.resources.resources.put(Resource.COIN,
                gameState.player.resources.resources.get(Resource.COIN) + times);
    }

    @Override
    public boolean isFinal() {
        return false;
    }

    @Override
    public String toString() {
        return String.format("convert %dW to %dC", times, times);
    }
}
