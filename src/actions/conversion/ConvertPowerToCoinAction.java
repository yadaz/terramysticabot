package actions.conversion;

import AI.GameState;
import actions.Action;
import com.google.common.collect.ImmutableMap;
import playerResources.Resource;
import playerResources.ResourceSet;

public class ConvertPowerToCoinAction extends Action {

    private final int times;

    public ConvertPowerToCoinAction(int times) {
        super(null);
        this.times = times;
    }

    @Override
    public ResourceSet getCost() {
        return new ResourceSet(ImmutableMap.<Resource, Integer>of());
    }

    @Override
    public void perform(GameState gameState) {
        gameState.player.resources.spendPower(times);

        gameState.player.resources.resources.put(Resource.COIN,
                gameState.player.resources.resources.get(Resource.COIN) + times);
    }

    @Override
    public boolean isFinal() {
        return false;
    }

    @Override
    public String toString() {
        return String.format("convert %dPW to %dC", times, times);
    }
}
