package actions.advance;

import AI.GameState;
import actions.Action;
import com.google.common.collect.ImmutableMap;
import main.Player;
import playerResources.Resource;
import playerResources.ResourceSet;

public class AdvanceDigAction extends Action {

    public AdvanceDigAction(final Player player) {
        super(player);
    }

    @Override
    public ResourceSet getCost() {
        return new ResourceSet(ImmutableMap.of(
                Resource.PRIEST, 1,
                Resource.WORKER, 2,
                Resource.COIN, 5
        ));
    }

    @Override
    public void perform(final GameState gameState) {
        gameState.player.resources.update(
                new ResourceSet(ImmutableMap.of(
                        Resource.PRIEST, 1,
                        Resource.WORKER, 2,
                        Resource.COIN, 5
                )), false
        );

        boolean canUpgrade = gameState.player.digLevel < gameState.player.faction.getMaximumDiggingLevel();
        if (canUpgrade) {
            gameState.player.digLevel++;
            gameState.player.resources.update(
                    Resource.VICTORY_POINT, 6
            );
        }
    }

    @Override
    public String toString() {
        return "advance dig";
    }

    @Override
    public boolean isFinal() {
        return true;
    }
}
