package actions.advance;

import AI.GameState;
import actions.Action;
import com.google.common.collect.ImmutableMap;
import main.Player;
import playerResources.Resource;
import playerResources.ResourceSet;

public class AdvanceShipAction extends Action {

    public AdvanceShipAction(final Player player) {
        super(player);
    }

    @Override
    public ResourceSet getCost() {
        return new ResourceSet(ImmutableMap.of(
                Resource.PRIEST, 1,
                Resource.COIN, 4
        ));
    }

    @Override
    public void perform(final GameState gameState) {
        gameState.player.resources.update(
                new ResourceSet(ImmutableMap.of(
                        Resource.PRIEST, 1,
                        Resource.COIN, 4
                )), false
        );

        boolean canUpgrade = gameState.player.shipLevel < gameState.player.faction.getMaximumShippingLevel();
        if (canUpgrade) {
            gameState.player.shipLevel++;
            gameState.player.resources.update(
                    Resource.VICTORY_POINT, gameState.player.shipLevel + 1
            );
        }
    }

    @Override
    public String toString() {
        return "advance ship";
    }

    @Override
    public boolean isFinal() {
        return true;
    }
}
