package actions;

import AI.GameState;
import main.Player;
import playerResources.ResourceSet;

public class BlockableActionMeta {

    private boolean isBlocked;
    public final ActionMeta actionMeta;

    public BlockableActionMeta(boolean isBlocked, ActionMeta actionMeta) {
        this.isBlocked =isBlocked;
        this.actionMeta = actionMeta;
    }

    public boolean isBlocked() {
        return isBlocked;
    }

    public void setBlocked(final boolean blocked) {
        isBlocked = blocked;
    }


    public static class BlockActionAction extends Action {

        private final BlockableActionMeta blockableActionMeta;

        public BlockActionAction(final Player player, BlockableActionMeta blockableActionMeta) {
            super(player);
            this.blockableActionMeta = blockableActionMeta;
        }

        @Override
        public ResourceSet getCost() {
            return ResourceSet.empty();
        }

        @Override
        public void perform(final GameState gameState) {
            if (blockableActionMeta.isBlocked) {
                throw new IllegalArgumentException("Action is blocked.");
            }

            blockableActionMeta.isBlocked = true;
        }

        @Override
        public String toString() {
            return "";
        }

        @Override
        public boolean isFinal() {
            return false;
        }
    }
}
