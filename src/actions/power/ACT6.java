package actions.power;

import AI.GameState;
import com.google.common.collect.ImmutableMap;
import playerResources.Resource;
import playerResources.ResourceSet;

import java.awt.*;

public class ACT6 extends PowerAction {

    private Point digSpot;
    private String tileName;

    public ACT6(boolean isTaken, Point digSpot, String tileName) {
        super(isTaken);
        this.digSpot = digSpot;
        this.tileName = tileName;
    }

    public ACT6(boolean isTaken) {
        super(isTaken);
    }

    @Override
    public ResourceSet getCost() {
        return new ResourceSet(ImmutableMap.of(
                Resource.P3, 6
        ));
    }

    @Override
    public String getName() {
        return "ACT6";
    }

    @Override
    protected int getPowerCost() {
        return 6;
    }

    @Override
    public PowerAction deepCopy() {
        return new ACT6(canTake(), digSpot, tileName);
    }

    @Override
    public void perform(final GameState gameState) {
        this.take();
        spendPower(gameState.player);
    }
}
