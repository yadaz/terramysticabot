package actions.power;

import AI.GameState;
import actions.Action;
import actions.ActionMeta;
import actions.conversion.BurnPowerAction;
import actions.primary.TransformAndBuildActionMeta;
import com.google.common.collect.ImmutableMap;
import playerResources.Resource;
import playerResources.ResourceSet;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class DiggingPowerActionMeta extends ActionMeta {
    private final String actionName;
    private final int numFreeDigs;
    private final int numPower;

    public DiggingPowerActionMeta(final GameState gameState,
                                  final String actionName,
                                  final int numFreeDigs,
                                  final int numPower
    ) {
        super(gameState);
        this.actionName = actionName;
        this.numFreeDigs = numFreeDigs;
        this.numPower = numPower;
    }

    @Override
    public List<List<Action>> getValidMoves(final ResourceSet alreadySpentResources) {
        List<List<Action>> validActions = new ArrayList<>();
        final int numToBurn = gameState.player.resources.canSpendPower(numPower);

        if (numToBurn < 0) { // not possible to burn that much power
            return Collections.emptyList();
        }

        final BurnPowerAction burnPowerAction = new BurnPowerAction(numToBurn);

        PowerAction pa = null;
        for (PowerAction powerAction : gameState.board.powerActions) {
            if (powerAction.getName().equals(actionName)) {
                pa = powerAction;
            }
        }

        final TransformAndBuildActionMeta transformAndBuildActionMeta = new TransformAndBuildActionMeta(gameState, numFreeDigs);
        transformAndBuildActionMeta.setAllowNoDig();

        for (List<Action> actions : transformAndBuildActionMeta.getValidMoves(
                new ResourceSet(
                        ImmutableMap.of(
                                Resource.P2, numToBurn,
                                Resource.P3, numPower - numToBurn
                        )
                )
        )) {
            List<Action> sequence = new ArrayList<>();
            sequence.add(burnPowerAction);
            sequence.add(pa);
            sequence.addAll(actions);
            validActions.add(sequence);
        }

        return validActions;
    }

    @Override
    public ActionMeta copy(final GameState gameState) {
        throw new IllegalArgumentException("Not allowed");
    }
}
