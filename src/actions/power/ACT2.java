package actions.power;

import AI.GameState;
import com.google.common.collect.ImmutableMap;
import playerResources.Resource;
import playerResources.ResourceSet;

public class ACT2 extends PowerAction {

    public ACT2(boolean isTaken) {
        super(isTaken);
    }

    @Override
    public ResourceSet getCost() {
        return new ResourceSet(ImmutableMap.of(
                Resource.P3, 3
        ));
    }

    @Override
    public String getName() {
        return "ACT2";
    }

    @Override
    protected int getPowerCost() {
        return 3;
    }

    @Override
    public PowerAction deepCopy() {
        return new ACT2(canTake());
    }

    @Override
    public void perform(final GameState gameState) {
        this.take();
        spendPower(gameState.player);

        gameState.player.resources.update(Resource.PRIEST, 1);
    }
}
