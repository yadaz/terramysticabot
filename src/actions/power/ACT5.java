package actions.power;

import AI.GameState;
import com.google.common.collect.ImmutableMap;
import playerResources.Resource;
import playerResources.ResourceSet;

public class ACT5 extends PowerAction {

    public ACT5(boolean isTaken) {
        super(isTaken);
    }

    @Override
    public ResourceSet getCost() {
        return new ResourceSet(ImmutableMap.of(
                Resource.P3, 4
        ));
    }

    @Override
    public String getName() {
        return "ACT5";
    }

    @Override
    protected int getPowerCost() {
        return 4;
    }

    @Override
    public PowerAction deepCopy() {
        return new ACT5(canTake());
    }

    @Override
    public void perform(final GameState gameState) {
        this.take();
        spendPower(gameState.player);
    }

    public String toString() {
        return "action ACT5";
    }
}
