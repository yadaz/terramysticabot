package actions.power;

import AI.GameState;
import com.google.common.collect.ImmutableMap;
import playerResources.Resource;
import playerResources.ResourceSet;

public class ACT3 extends PowerAction {

    public ACT3(boolean isTaken) {
        super(isTaken);
    }

    @Override
    public ResourceSet getCost() {
        return new ResourceSet(ImmutableMap.of(
                Resource.P3, 4
        ));
    }

    @Override
    public String getName() {
        return "ACT3";
    }

    @Override
    protected int getPowerCost() {
        return 4;
    }

    @Override
    public PowerAction deepCopy() {
        return new ACT3(canTake());
    }

    @Override
    public void perform(final GameState gameState) {
        this.take();
        spendPower(gameState.player);

        gameState.player.resources.update(Resource.WORKER, 2);
    }
}
