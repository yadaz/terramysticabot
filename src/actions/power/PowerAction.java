package actions.power;

import actions.Action;
import com.google.common.collect.ImmutableMap;
import main.Player;
import playerResources.Resource;
import playerResources.ResourceSet;

public abstract class PowerAction extends Action {

    private boolean isTaken;

    public PowerAction(boolean isTaken) {
        super(null);
        this.isTaken = isTaken;
    }

    public void take() {
        isTaken = true;
    }

    public boolean canTake() {
        return !isTaken;
    }

    public void setTake(boolean b) {
        this.isTaken = b;
    }

    @Override
    public ResourceSet getCost() {
        return new ResourceSet(ImmutableMap.of(
                Resource.P3, getPowerCost()
        ));
    }

    public abstract String getName();

    @Override
    public boolean isFinal() {
        return true;
    }

    // How much power it costs, for example, ACT3 (workers) costs 4 power
    protected abstract int getPowerCost();

    protected void spendPower(Player p) {
        p.resources.update(Resource.P3, -1 * getPowerCost());
        p.resources.update(Resource.P1, getPowerCost());
    }

    @Override
    public String toString() {
        return "action " + this.getClass().getSimpleName();
    }

    public abstract PowerAction deepCopy();
}
