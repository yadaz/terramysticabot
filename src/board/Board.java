package board;

import actions.power.PowerAction;
import main.Player;

import java.awt.*;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

public class Board {
    public Tile[][] tiles;
    public List<BonusTile> bonusTiles = new ArrayList<>();
    public List<PowerAction> powerActions = new ArrayList<>();

    public int roundNumber;
    public boolean lastPlayerUnpassed;

    public Board(Tile[][] tiles) {
        this.tiles = tiles;
    }

    public static Tile[][] setupBoard() {
        Tile[][] tiles = new Tile[9][13];
        tiles[0][0] = new Tile (new Point(0, 0), "A1", Color.BROWN);
        tiles[0][1] = new Tile (new Point(0, 1), "A2", Color.GRAY);
        tiles[0][2] = new Tile (new Point(0, 2), "A3", Color.GREEN);
        tiles[0][3] = new Tile (new Point(0, 3), "A4", Color.BLUE);
        tiles[0][4] = new Tile (new Point(0, 4), "A5", Color.YELLOW);
        tiles[0][5] = new Tile (new Point(0, 5), "A6", Color.RED);
        tiles[0][6] = new Tile (new Point(0, 6), "A7", Color.BROWN);
        tiles[0][7] = new Tile (new Point(0, 7), "A8", Color.BLACK);
        tiles[0][8] = new Tile (new Point(0, 8), "A9", Color.RED);
        tiles[0][9] = new Tile (new Point(0, 9), "A10", Color.GREEN);
        tiles[0][10] = new Tile (new Point(0, 10), "A11", Color.BLUE);
        tiles[0][11] = new Tile (new Point(0, 11), "A12", Color.RED);
        tiles[0][12] = new Tile (new Point(0, 12), "A13", Color.BLACK);

        tiles[1][0] = new Tile (new Point(1, 0), "B1", Color.YELLOW);
        tiles[1][1] = new Tile (new Point(1, 1), "B2", Color.WHITE);
        tiles[1][2] = new Tile (new Point(1, 2), "B3", Color.WHITE);
        tiles[1][3] = new Tile (new Point(1, 3), "B4", Color.BROWN);
        tiles[1][4] = new Tile (new Point(1, 4), "B5", Color.BLACK);
        tiles[1][5] = new Tile (new Point(1, 5), "B6", Color.WHITE);
        tiles[1][6] = new Tile (new Point(1, 6), "B7", Color.WHITE);
        tiles[1][7] = new Tile (new Point(1, 7), "B8", Color.YELLOW);
        tiles[1][8] = new Tile (new Point(1, 8), "B9", Color.BLACK);
        tiles[1][9] = new Tile (new Point(1, 9), "B10", Color.WHITE);
        tiles[1][10] = new Tile (new Point(1, 10), "B11", Color.WHITE);
        tiles[1][11] = new Tile (new Point(1, 11), "B12", Color.YELLOW);
        tiles[1][12] = null;

        tiles[2][0] = new Tile (new Point(2, 0), "C1", Color.WHITE);
        tiles[2][1] = new Tile (new Point(2, 1), "C2", Color.WHITE);
        tiles[2][2] = new Tile (new Point(2, 2), "C3", Color.BLACK);
        tiles[2][3] = new Tile (new Point(2, 3), "C4", Color.WHITE);
        tiles[2][4] = new Tile (new Point(2, 4), "C5", Color.GRAY);
        tiles[2][5] = new Tile (new Point(2, 5), "C6", Color.WHITE);
        tiles[2][6] = new Tile (new Point(2, 6), "C7", Color.GREEN);
        tiles[2][7] = new Tile (new Point(2, 7), "C8", Color.WHITE);
        tiles[2][8] = new Tile (new Point(2, 8), "C9", Color.GREEN);
        tiles[2][9] = new Tile (new Point(2, 9), "C10", Color.WHITE);
        tiles[2][10] = new Tile (new Point(2, 10), "C11", Color.GRAY);
        tiles[2][11] = new Tile (new Point(2, 11), "C12", Color.WHITE);
        tiles[2][12] = new Tile (new Point(2, 12), "C13", Color.WHITE);

        tiles[3][0] = new Tile (new Point(3, 0), "D1", Color.GREEN);
        tiles[3][1] = new Tile (new Point(3, 1), "D2", Color.BLUE);
        tiles[3][2] = new Tile (new Point(3, 2), "D3", Color.YELLOW);
        tiles[3][3] = new Tile (new Point(3, 3), "D4", Color.WHITE);
        tiles[3][4] = new Tile (new Point(3, 4), "D5", Color.WHITE);
        tiles[3][5] = new Tile (new Point(3, 5), "D6", Color.RED);
        tiles[3][6] = new Tile (new Point(3, 6), "D7", Color.BLUE);
        tiles[3][7] = new Tile (new Point(3, 7), "D8", Color.WHITE);
        tiles[3][8] = new Tile (new Point(3, 8), "D9", Color.RED);
        tiles[3][9] = new Tile (new Point(3, 9), "D10", Color.WHITE);
        tiles[3][10] = new Tile (new Point(3, 10), "D11", Color.RED);
        tiles[3][11] = new Tile (new Point(3, 11), "D12", Color.BROWN);
        tiles[3][12] = null;

        tiles[4][0] = new Tile (new Point(4, 0), "E1", Color.BLACK);
        tiles[4][1] = new Tile (new Point(4, 1), "E2", Color.BROWN);
        tiles[4][2] = new Tile (new Point(4, 2), "E3", Color.RED);
        tiles[4][3] = new Tile (new Point(4, 3), "E4", Color.BLUE);
        tiles[4][4] = new Tile (new Point(4, 4), "E5", Color.BLACK);
        tiles[4][5] = new Tile (new Point(4, 5), "E6", Color.BROWN);
        tiles[4][6] = new Tile (new Point(4, 6), "E7", Color.GRAY);
        tiles[4][7] = new Tile (new Point(4, 7), "E8", Color.YELLOW);
        tiles[4][8] = new Tile (new Point(4, 8), "E9", Color.WHITE);
        tiles[4][9] = new Tile (new Point(4, 9), "E10", Color.WHITE);
        tiles[4][10] = new Tile (new Point(4, 10), "E11", Color.GREEN);
        tiles[4][11] = new Tile (new Point(4, 11), "E12", Color.BLACK);
        tiles[4][12] = new Tile (new Point(4, 12), "E13", Color.BLUE);

        tiles[5][0] = new Tile (new Point(5, 0), "F1", Color.GRAY);
        tiles[5][1] = new Tile (new Point(5, 1), "F2", Color.GREEN);
        tiles[5][2] = new Tile (new Point(5, 2), "F3", Color.WHITE);
        tiles[5][3] = new Tile (new Point(5, 3), "F4", Color.WHITE);
        tiles[5][4] = new Tile (new Point(5, 4), "F5", Color.YELLOW);
        tiles[5][5] = new Tile (new Point(5, 5), "F6", Color.GREEN);
        tiles[5][6] = new Tile (new Point(5, 6), "F7", Color.WHITE);
        tiles[5][7] = new Tile (new Point(5, 7), "F8", Color.WHITE);
        tiles[5][8] = new Tile (new Point(5, 8), "F9", Color.WHITE);
        tiles[5][9] = new Tile (new Point(5, 9), "F10", Color.BROWN);
        tiles[5][10] = new Tile (new Point(5, 10), "F11", Color.GRAY);
        tiles[5][11] = new Tile (new Point(5, 11), "F12", Color.BROWN);
        tiles[5][12] = null;

        tiles[6][0] = new Tile (new Point(6, 0), "G1", Color.WHITE);
        tiles[6][1] = new Tile (new Point(6, 1), "G2", Color.WHITE);
        tiles[6][2] = new Tile (new Point(6, 2), "G3", Color.WHITE);
        tiles[6][3] = new Tile (new Point(6, 3), "G4", Color.GRAY);
        tiles[6][4] = new Tile (new Point(6, 4), "G5", Color.WHITE);
        tiles[6][5] = new Tile (new Point(6, 5), "G6", Color.RED);
        tiles[6][6] = new Tile (new Point(6, 6), "G7", Color.WHITE);
        tiles[6][7] = new Tile (new Point(6, 7), "G8", Color.GREEN);
        tiles[6][8] = new Tile (new Point(6, 8), "G9", Color.WHITE);
        tiles[6][9] = new Tile (new Point(6, 9), "G10", Color.YELLOW);
        tiles[6][10] = new Tile (new Point(6, 10), "G11", Color.BLACK);
        tiles[6][11] = new Tile (new Point(6, 11), "G12", Color.BLUE);
        tiles[6][12] = new Tile (new Point(6, 12), "G13", Color.YELLOW);

        tiles[7][0] = new Tile (new Point(7, 0), "H1", Color.YELLOW);
        tiles[7][1] = new Tile (new Point(7, 1), "H2", Color.BLUE);
        tiles[7][2] = new Tile (new Point(7, 2), "H3", Color.BROWN);
        tiles[7][3] = new Tile (new Point(7, 3), "H4", Color.WHITE);
        tiles[7][4] = new Tile (new Point(7, 4), "H5", Color.WHITE);
        tiles[7][5] = new Tile (new Point(7, 5), "H6", Color.WHITE);
        tiles[7][6] = new Tile (new Point(7, 6), "H7", Color.BLUE);
        tiles[7][7] = new Tile (new Point(7, 7), "H8", Color.BLACK);
        tiles[7][8] = new Tile (new Point(7, 8), "H9", Color.WHITE);
        tiles[7][9] = new Tile (new Point(7, 9), "H10", Color.GRAY);
        tiles[7][10] = new Tile (new Point(7, 10), "H11", Color.BROWN);
        tiles[7][11] = new Tile (new Point(7, 11), "H12", Color.GRAY);
        tiles[7][12] = null;

        tiles[8][0] = new Tile (new Point(8, 0), "I1", Color.RED);
        tiles[8][1] = new Tile (new Point(8, 1), "I2", Color.BLACK);
        tiles[8][2] = new Tile (new Point(8, 2), "I3", Color.GRAY);
        tiles[8][3] = new Tile (new Point(8, 3), "I4", Color.BLUE);
        tiles[8][4] = new Tile (new Point(8, 4), "I5", Color.RED);
        tiles[8][5] = new Tile (new Point(8, 5), "I6", Color.GREEN);
        tiles[8][6] = new Tile (new Point(8, 6), "I7", Color.YELLOW);
        tiles[8][7] = new Tile (new Point(8, 7), "I8", Color.RED);
        tiles[8][8] = new Tile (new Point(8, 8), "I9", Color.GRAY);
        tiles[8][9] = new Tile (new Point(8, 9), "I10", Color.WHITE);
        tiles[8][10] = new Tile (new Point(8, 10), "I11", Color.BLUE);
        tiles[8][11] = new Tile (new Point(8, 11), "I12", Color.GREEN);
        tiles[8][12] = new Tile (new Point(8, 12), "I13", Color.RED);
        return tiles;
    }

    public void setBonusTiles(List<BonusTile> bonusTiles) {
        this.bonusTiles = bonusTiles;
    }

    public void setTile(final int column, final int row, final Tile tile) {
        tiles[column][row] = tile;
    }

    public void setLastPlayerUnpassed(final boolean lastPlayerUnpassed) {
        this.lastPlayerUnpassed = lastPlayerUnpassed;
    }

    public List<Tile> getReachableAreas(Player p) {
        // TODO: figure out, based on shipping, etc. all the reachable tiles.
        final Set<Tile> myTiles = p.ownedSpaces;
        final Set<Tile> reachableTiles = new HashSet<Tile>();

        for (final Iterator<Tile> i = myTiles.iterator(); i.hasNext(); ) {
            reachableTiles.addAll(getAdjacentSpaces(i.next(), p.shipLevel));
        }

        final List<Tile> reachableTilesList = new ArrayList<Tile>();
        reachableTilesList.addAll(reachableTiles);

        return reachableTilesList;
    }

    private Set<Tile> getAdjacentSpaces (final Tile coordinate, final int shippingLevel) {
        Set<Tile> adjacentSpaces = getAdjacentSpacesHelper(coordinate, shippingLevel, new HashSet<Tile>());

        for (final Iterator<Tile> i = adjacentSpaces.iterator(); i.hasNext(); ) {
            Tile tile = i.next();

            if (tile.color == Color.WHITE) {
                i.remove();
            }

            if (tile.building != null) {
                i.remove();
            }
        }

        return adjacentSpaces;
    }

    public void setRoundNumber(int roundNumber) {
        this.roundNumber = roundNumber;
    }

    private Set<Tile> getAdjacentSpacesHelper (final Tile tile, final int shippingLevel, final Set<Tile> currentAdjacentSpaces) {

        if (shippingLevel < 0) {
            return currentAdjacentSpaces;
        }

        final int x = tile.coordinate.x;
        final int y = tile.coordinate.y;

        final List<Point> potentialSpaces = new ArrayList<Point>();

        potentialSpaces.add(new Point(x, y - 1));
        potentialSpaces.add(new Point(x, y + 1));
        potentialSpaces.add(new Point(x - 1, y));
        potentialSpaces.add(new Point(x + 1, y));

        if ((x % 2) == 0) {
            potentialSpaces.add(new Point(x - 1, y - 1));
            potentialSpaces.add(new Point(x + 1, y - 1));
        } else {
            potentialSpaces.add(new Point(x - 1, y + 1));
            potentialSpaces.add(new Point(x + 1, y + 1));
        }

        for (final Iterator<Point> i = potentialSpaces.iterator(); i.hasNext(); ) {
            final Point currentPoint = i.next();
            if (isValidCoordinate(currentPoint)) {
                currentAdjacentSpaces.add(tiles[currentPoint.x][currentPoint.y]);

                if (tiles[currentPoint.x][currentPoint.y].color == Color.WHITE) {
                    currentAdjacentSpaces.addAll(getAdjacentSpacesHelper(tiles[currentPoint.x][currentPoint.y], shippingLevel-1, currentAdjacentSpaces));
                }
            }

        }

        return currentAdjacentSpaces;
    }

    private boolean isValidCoordinate (final Point coordinate) {
        final int row = coordinate.x;
        final int col = coordinate.y;

        if ((row < 0) || (col < 0) || (row > 8) || (col > 12)) {
            return false;
        }

        if ((col > 11) && ((row % 2) == 1)) {
            return false;
        }

        return true;
    }

    public Board deepCopy() {
        Tile[][] copyOfTiles = new Tile[this.tiles.length][this.tiles[0].length];
        for (int i = 0; i < tiles.length; i++) {
            for (int j = 0; j < tiles[i].length; j++) {
                if (tiles[i][j] != null) {
                    final Tile copiedTile = tiles[i][j].deepCopy();
                    copyOfTiles[i][j] = copiedTile;
                }
            }
        }

        final Board board = new Board(copyOfTiles);
        board.bonusTiles = this.bonusTiles;

        List<PowerAction> copiedPowerActions = new ArrayList<>();
        for (PowerAction powerAction : powerActions) {
            copiedPowerActions.add(powerAction.deepCopy());
        }
        board.powerActions = copiedPowerActions;
        return board;
    }
}
