package board;

/**
 * @author franklin
 */

public class BonusTile {

    private String tileName;
    private int coins;
    private boolean isAvailable;

    public BonusTile(String tileName, int coins, boolean isAvailable) {
        this.tileName = tileName;
        this.coins = coins;
        this.isAvailable = isAvailable;
    }

    public String getTileName() {
        return tileName;
    }

    public int getCoins() {
        return coins;
    }

    public boolean isAvailable() {
        return isAvailable;
    }
}
