package board;

/**
 *
 */
public enum Color {
    BLUE, GREEN, GRAY, RED, YELLOW, BROWN, BLACK, VOLCANO, WHITE;

    public int getMinDistanceTo(Color otherColor) {
        if (otherColor.equals(Color.WHITE) || this.equals(Color.WHITE)) {
            throw new IllegalArgumentException("Cannot terraform rivers");
        }

        int myEnumVal = this.ordinal();
        int otherEnumVal = otherColor.ordinal();

        // check  blue -> green -> ... transformation direction
        // then, check green -> blue -> ... transformation direction

        return Math.min(
                otherEnumVal - myEnumVal < 0 ? otherEnumVal + 7 - myEnumVal : otherEnumVal - myEnumVal,
                myEnumVal - otherEnumVal < 0 ? myEnumVal + 7 - otherEnumVal : myEnumVal - otherEnumVal
        );
    }
}