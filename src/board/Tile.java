package board;

import com.google.common.base.Objects;
import structures.Building;

import java.awt.Point;

public class Tile {

    public Point coordinate;
    public String name;
    public Color color;
    public boolean bridgable;
    public boolean hasNeighbor;
    public Building building = null;

    public Tile () {
        coordinate = new Point(0, 0);
        name = "A1";
        color = Color.BLUE;
    }

    public Tile(Point coordinate,
                String name,
                Color color) {
        this.coordinate = coordinate;
        this.name = name;
        this.color = color;
    }

    public Point getCoordinate () {
        return coordinate;
    }

    public void setCoordinate (Point coordinate) {
        this.coordinate = coordinate;
    }

    public String getName () {
        return name;
    }

    public void setName (String name) {
        this.name = name;
    }

    public Color getColor () {
        return color;
    }

    public void setColor (Color color) {
        this.color = color;
    }

    public void setBridgable (boolean bridgable) {
        this.bridgable = bridgable;
    }

    public boolean getBridgable() {
        return bridgable;
    }

    public void setHasNeighbor(boolean hasNeighbor) {
        this.hasNeighbor = hasNeighbor;
    }

    public boolean getHasNeighbor() {
        return hasNeighbor;
    }

    public boolean isBlocked() {
        return building != null;
    }

    public void setBuilding(Building building) {
        this.building = building;
    }

    public Building getBuilding() {
        return building;
    }

    public Tile deepCopy() {
        final Tile tile = new Tile(coordinate, name, color);
        tile.building = this.building;
        tile.bridgable = this.bridgable;
        tile.hasNeighbor = this.hasNeighbor;
        return tile;
    }

    public String toString() {
        return this.name + "           " + this.coordinate.toString();
    }

    @Override
    public boolean equals(final Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        final Tile tile = (Tile) o;
        return Objects.equal(coordinate, tile.coordinate);
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(coordinate);
    }
}