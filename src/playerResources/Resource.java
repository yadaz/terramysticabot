package playerResources;

public enum Resource {
        COIN,
        VICTORY_POINT,
        WORKER,
        PRIEST,
        POWER, // used for giving income
        P1, P2, P3, // used for keeping track of how much power is in each bowl. Bowl 3 is the highest one.
        PRIESTS_AVAILABLE,
        BRIDGE_COUNT;
}
