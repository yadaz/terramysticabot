package playerResources;

import actions.Action;
import actions.conversion.BurnPowerAction;
import actions.conversion.ConvertPowerToCoinAction;
import actions.conversion.ConvertWorkerToCoinAction;

import java.util.*;

public class ResourceSet {
    public Map<Resource, Integer> resources;

    public ResourceSet(Map<Resource, Integer> resources) {
        this.resources = new HashMap<>(resources);
    }

    public void update(Resource s, int amount) {
        if (!resources.containsKey(s)) {
            resources.put(s, amount);
        }
        resources.put(s, resources.get(s) + amount);
    }

    public void update(ResourceSet cost, boolean increase) {
        for (Map.Entry<Resource, Integer> costResourceEntry : cost.resources.entrySet()) {
            Integer numRemaining = resources.get(costResourceEntry.getKey());
            if (numRemaining == null) {
                numRemaining = 0;
            }
            if (increase) {
                numRemaining = numRemaining + costResourceEntry.getValue();
            } else {
                numRemaining = numRemaining - costResourceEntry.getValue();
            }

            if (numRemaining < 0) {
                throw new IllegalArgumentException("YOU DONT HAVE ENOUGH " + costResourceEntry.getKey());
            }

            resources.put(costResourceEntry.getKey(), numRemaining);
        }
    }

    public void updateFriendly(ResourceSet cost, boolean increase) {
        for (Map.Entry<Resource, Integer> costResourceEntry : cost.resources.entrySet()) {
            Integer numRemaining = resources.get(costResourceEntry.getKey());
            if (numRemaining == null) {
                numRemaining = 0;
            }
            if (increase) {
                numRemaining = numRemaining + costResourceEntry.getValue();
            } else {
                numRemaining = numRemaining - costResourceEntry.getValue();
            }

            resources.put(costResourceEntry.getKey(), numRemaining);
        }
    }

    /**
     * if all good, returns null.
     *
     * if missing something, returns what's missing
     */
    public ResourceSet canSpend(ResourceSet cost) {
        ResourceSet missing = ResourceSet.empty();
        boolean ok = true;
        for (Map.Entry<Resource, Integer> costResourceEntry : cost.resources.entrySet()) {
            Integer numRemaining = resources.get(costResourceEntry.getKey());
            numRemaining = numRemaining - costResourceEntry.getValue();

            if (numRemaining < 0) {
                missing.update(costResourceEntry.getKey(), numRemaining * -1);
                ok  = false;
            }
        }
        if (!ok) {
            return missing;
        }
        return null;
    }

    /**
     * "this" is the total cost of an Action chain
     *
     * Returns a list of the lists of possible conversion actions
     *
     * For example, if you're 2 gold short, it will return
     * {
     *     {convert 2W to 2C},
     *     {convert PW to C, convert W to C}, // TODO: Not implemented yet
     *     {convert 2PW to 2C}                // TODO: Not implemented yet
     * }
     *
     * if it's not possible to convert things to be able to spend that much, returns null
     * if no conversions are needed, returns an empty list
     */
    public List<List<Action>> getConversionActions(ResourceSet resourceSet) {
        ResourceSet playerResources = this.copy();
        playerResources.updateFriendly(resourceSet, false);

        Map<Resource, Integer> numMissing = new HashMap<>();
        for (Map.Entry<Resource, Integer> resource : playerResources.resources.entrySet()) {
            if (resource.getValue() < 0) {
                // TODO: Only allowed to convert to coins now
                if (!resource.getKey().equals(Resource.COIN) &&
                        !resource.getKey().equals(Resource.P3)) {
                    return null;
                }
                numMissing.put(resource.getKey(), resource.getValue() * -1);
            }
        }

        if (numMissing.isEmpty()) {
            Action dummyAction = new BurnPowerAction(0);
            List<List<Action>> res = new ArrayList<>();
            res.add(Collections.singletonList(dummyAction));
            return res;
            // HACK!
            // this make for loops over each conversion action work!
        }

        BurnPowerAction burnPowerAction = new BurnPowerAction(0);
        // can I burn power?
        if (numMissing.containsKey(Resource.P3)) {
            final int canSpendPower = canSpendPower(numMissing.get(Resource.P3));
            if (canSpendPower == -1) {
                return null;
            } else {
                burnPowerAction = new BurnPowerAction(canSpendPower);
            }
        }

        // am I missing workers?
        if (numMissing.containsKey(Resource.WORKER)) {
            return null; //TODO: Not possible to convert priests/power to workers yet!
        }

        List<List<Action>> coinConversions = new ArrayList<>();
        // am I missing gold?
        if (numMissing.containsKey(Resource.COIN)) {
            // W to C conversion
            int totalWorkers = playerResources.resources.keySet().contains(Resource.WORKER) ? playerResources.resources.get(Resource.WORKER) : 0;

            if (totalWorkers > numMissing.get(Resource.COIN)) {
                final Action convertWorkerToCoinAction = new ConvertWorkerToCoinAction(numMissing.get(Resource.COIN));
                coinConversions.add(Collections.singletonList(convertWorkerToCoinAction));
            }

            // PW to C conversion
            int p3Remaining = this.resources.get(Resource.P3) - playerResources.resources.get(Resource.P3);

            if (numMissing.containsKey(Resource.P3) && p3Remaining > numMissing.get(Resource.COIN)) {
                final Action convertWorkerToCoinAction = new ConvertPowerToCoinAction(numMissing.get(Resource.COIN));
                coinConversions.add(Collections.singletonList(convertWorkerToCoinAction));
            }

            // TODO: PW to C conversion with burning
        }

        List<List<Action>> compiledConversions = new ArrayList<>();

        for (List<Action> coinConversion : coinConversions) {
            final ArrayList<Action> actions = new ArrayList<>();
            actions.add(burnPowerAction);
            actions.addAll(coinConversion);
            compiledConversions.add(actions);
        }

        return compiledConversions;
    }

    /**
     * returns number of power that needs to be burned.
     *
     * -1 if not possible
     */
    public int canSpendPower(int numPower) {
        int playerP2 = resources.get(Resource.P2);
        int playerP3 = resources.get(Resource.P3);

        if (playerP3 > numPower) {
            return 0;
        }

        int numNeededToBeBurned = 2 * (numPower - playerP3);
        if (numNeededToBeBurned > playerP2) {
            return -1;
        }

        return numNeededToBeBurned / 2;
    }

    public void spendPower(int numPower) {
        final int P3 = resources.get(Resource.P3);
        if (P3 < numPower) {
            throw new IllegalArgumentException("cannot spend that much power");
        }
        resources.put(Resource.P3, P3 - numPower);
        resources.put(Resource.P1, resources.get(Resource.P1) + numPower);
    }

    public static ResourceSet empty() {
        return new ResourceSet(new HashMap<Resource, Integer>());
    }

    public ResourceSet copy() {
        return new ResourceSet(new HashMap<>(resources));
    }
}
